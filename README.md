# TNS.js

[![npm version](https://img.shields.io/npm/v/@tns-money/tns.js.svg?label=version)](https://www.npmjs.com/package/@tns-money/tns.js)
[![package license](https://img.shields.io/npm/l/@tns-money/tns.js.svg)](https://www.npmjs.com/package/@tns-money/tns.js)
<!-- [![npm downloads](https://img.shields.io/npm/dm/@tns-money/tns.js.svg)](https://www.npmjs.com/package/@tns-money/tns.js) -->

TNS.js is a JavaScript SDK for building applications that interacts with Terra Name Service from within JavaScript runtimes, such as web browsers, server backends, and on mobile through React Native. TNS.js provides simple abstractions over core functionality such as queries and transaction executions.

## Table of Contents
- [Getting Started](#getting-started)
  - [Requirements](#requirements)
  - [Installation](#installation)
- [Usage](#usage)
  - [`TNS` object](#tns-object)
  - [`Name` object](#name-object)
    - [Query](#query)
    - [Batch Query](#batch-query)
    - [Execution](#execution)
- [APIs](#apis)
  - [TNS](#tns)
  - [Name](#name)
- [Examples](#examples)


## Getting Started

A walk through of the steps to get started with the Terra Name Service SDK alongside with minimum requirements are provided below.

### Requirements
- Node.js v14+
- NPM or Yarn
- `@terra-money/terra.js` >= v3.0.2

### Installation
TNS.js is available as a package on NPM and is intended to to be used alongside Terra.js.

Add both:
- `@terra-money/terra.js`
- `@tns-money/tns.js`

To your JavaScript project's package.json as dependencies using your preferred package manager:

```sh
npm install @terra-money/terra.js @tns-money/tns.js
```

or

```sh
yarn add @terra-money/terra.js @tns-money/tns.js
```

## Usage

TNS.js can be used in Node.js, as well as inside the browser.

The most simplest usecase of TNS.js is to resolve a Terra Address from a TNS name. For example, resolving which Terra Address `"bucky.ust"` points to:

```ts
import { TNS } from '@tns-money/tns.js'

const tns = new TNS()

await tns.name('bucky.ust').getTerraAddress() // "terra17err4n4...m35eRv1c3"
```

### `TNS` object

`TNS` is the main interface for querying data, creating `Name` objects and also for name-independent operations such as getting a Reverse Record from a Terra Address.

```ts
import { TNS } from '@tns-money/tns.js'

const tns = new TNS()

// Create a Name object
tns.name('bucky.ust') // Name object

// Get TNS name from terra address (Reverse Record)
await tns.getName('terra17err4n4...m35eRv1c3') // "bucky.ust"
```

You can also specify TNS config through TNS object constructor (optional):

```ts
import { TNS } from '@tns-money/tns.js'

const tns = new TNS({
  /**
   * Network name indicates which contract address to be used.
   * (Registry, Resolver, Controller, etc.)
   */
  network: 'mainnet' | 'testnet',

  /**
   * Wallet address of the transaction sender.
   * (Required when making execution transaction)
   */
  walletAddress: 'terra17err4n4...m35eRv1c3',

  /**
   * Custom Terra Mantle indexer URL.
   */
  mantleUrl: 'https://mantle.terra.dev'
})
```

### `Name` object

`Name` is the main interface for querying and building execution transactions. It acts as an abstraction layer over multiple contracts (Registry, Resolver, Registrar and Controller) with 2 main use cases:

- **Query**: Runs smart contract queries through Mantle indexer with GraphQL
- **Execute**: Creates `MsgExecuteContract` objects to be used in transactions

#### Query

```ts
const name = tns.name('bucky.ust')

await name.getOwner() // "terra17err4n4...m35eRv1c3"
await name.getTextData('twitter') // "https://twitter.com/tns_money"
await name.getImage() // "data:image/svg+xml;base64,..."
```

#### Batch Query
With the power of Mantle Indexer, you can do a batch query to save network requests using `name.query(...)` method:

```ts
const name = tns.name('bucky.ust')

const { data } = await name.query(builder =>
  builder
    .getExpires()
    .getTerraAddress()
    .isAvailable()
    .getOwner()
    .getEditor()
    .getImage()
    .getTextData('twitter', 'email')
)

console.log(data)
// {
//   expires: 1667315503,
//   terraAddress: 'terra17err4n4...m35eRv1c3',
//   isAvailable: false,
//   owner: 'terra17err4n4...m35eRv1c3',
//   editor: 'terra17err4n4...m35eRv1c3',
//   image: 'data:image/svg+xml;base64,...',
//   textData: {
//     twitter: 'https://twitter.com/tns_money',
//     email: 'admin@tns.money'
//   }
// }
```

#### Execution
TNS.js provides a simple way to create transaction messages (`MsgExecuteContract`), so that it is independent from libraries used to broadcast transactions.

First, you need to specify the `walletAddress` of the transaction sender when creating a `TNS` instance (required):

```ts
// walletAddress will be used as a transaction sender in MsgExecuteContract
const tns = new TNS({ walletAddress: 'terra17err4n4...m35eRv1c3' })
const name = tns.name('bucky.ust')
```

Then, create a transaction message using the provided API:
```ts
// Create a transaction message (MsgExecuteContract)
const setTerraAddressMsg = await name.setTerraAddress('terra1new...address')
```

With the returned `MsgExecuteContract`, you can sign and broadcast it with any libraries of your preference:

```ts
// Broadcast transaction using @terra-money/terra.js LCDClient
const lcdClient = new LCDClient({
  URL: 'https://lcd.terra.dev',
  chainID: 'columbus-5'
})

const wallet = lcdClient.wallet(new MnemonicKey({ mnemonic: '...' }))

const tx = await wallet.createAndSignTx({
  msgs: [setTerraAddressMsg],
  fee: new StdFee(200000, '50000uusd'),
})

await lcdClient.tx.broadcastAsync(tx)
```

or

```ts
// Broadcast transaction using @terra-money/wallet-provider
const connectedWallet = useConnectedWallet()

await connectedWallet.post({
  msgs: [setTerraAddressMsg],
  fee: new StdFee(200000, '50000uusd'),
})
```

## APIs

### TNS
| Method                                | Return   | Description                                 |
| ------------------------------------- | -------- | ------------------------------------------- |
| `name(name: string)`                  | `Name`   | Create a `Name` object                      |
| `await getName(terraAddress: string)` | `string` | Get TNS name of the specified terra address |

### Name

| Method                           | Return    | Description                            |
| -------------------------------- | --------- | -------------------------------------- |
| `await isAvailable()`            | `boolean` | Get availability status                |
| `await getExpires()`             | `number`  | Get expiration date in Unix timestamp  |
| `await getImage()`               | `string`  | Get this TNS image in base64 string    |
| `await getOwner()`               | `string`  | Get TNS owner address                  |
| `await getEditor()`              | `string`  | Get this TNS editor address            |
| `await getTerraAddress()`        | `string`  | Get Terra Address this TNS resolves to |
| `await getTextData(key: string)` | `string`  | Get Text Data of the specified key     |
| `await getRegistrar()`           | `string`  | Get Registrar address (NFT)            |
| `await getResolver()`            | `string`  | Get Resolver address                   |


## Examples
You can find example usages of TNS.js via:
- [React Example](./example/react/src/App.tsx)
- [NodeJS Example](./example/nodejs/query.ts)
