export type NetworkName = 'mainnet' | 'testnet'

export type Result = Partial<{
  isAvailable: boolean
  expires: number
  gracePeriod: number
  owner: string
  image: string
  // TODO: Add config types
  config: any
  registrar: string
  editor: string
  resolver: string
  ttl: number
  terraAddress: string
  textData: { [key: string]: string }
  contentHash: string
  address: { [key: number]: string }
  commitmentTimestamp: number
  rentPrice: string
}>

export type RegistryRecord = {
  owner: string
  resolver: string
  ttl: number
}

export type CreateCommitmentOptions = {
  /** Commitment secret. */
  secret: string

  /**
   * Owner and Editor of this name.
   *
   * @defaultValue TNS's walletAddress
   */
  owner?: string

  /**
   * Terra address this name should resolve to.
   *
   * @defaultValue TNS's walletAddress
   */
  address?: string

  /**
   * Resolver address this name should use.
   *
   * @defaultValue TNS's default resolver
   */
  resolver?: string
}

export type RegisterOptions = {
  /** Secret associated to the commitment. */
  secret: string

  /** Registration duration (in seconds). */
  duration: number

  /**
   * Owner of this TNS name.
   *
   * @defaultValue TNS's walletAddress
   */
  owner?: string

  /**
   * Resolver address this name should be set to.
   *
   * @defaultValue TNS's default resolver
   */
  resolver?: string

  /**
   * Terra address this name should resolve to.
   *
   * @defaultValue TNS's walletAddress
   */
  address?: string
}

export type BaseName = 'ust'
