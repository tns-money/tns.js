import ReactDOM from 'react-dom'
import { App } from './App'
import { getChainOptions, WalletProvider } from '@terra-money/wallet-provider'

getChainOptions().then(chainOptions => {
  const app = document.getElementById('app')

  ReactDOM.render(
    <WalletProvider {...chainOptions}>
      <App />
    </WalletProvider>,
    app
  )
})
