import { Coin, Coins, Fee, LCDClient } from '@terra-money/terra.js'
import { ConnectType, useConnectedWallet, useWallet } from '@terra-money/wallet-provider'
import dayjs from 'dayjs'
import duration from 'dayjs/plugin/duration'
import relativeTime from 'dayjs/plugin/relativeTime'
import delay from 'delay'
import React, { FormEvent, useEffect, useMemo, useState } from 'react'
import { NetworkName, Result, TNS } from 'tns.js'

dayjs.extend(duration)
dayjs.extend(relativeTime)

export const App = () => {
  const [nameInput, setNameInput] = useState('tomato.ust')
  const [name, setName] = useState(nameInput)
  const [data, setData] = useState<Result>(null)

  const connectedWallet = useConnectedWallet()
  const { network, connect, disconnect } = useWallet()

  const tns = useMemo(() => new TNS({ network: network.name as NetworkName }), [network])

  const lcdClient = useMemo(
    () => new LCDClient({ URL: network.lcd, chainID: network.chainID }),
    [network]
  )

  const fetchData = async (e?: FormEvent) => {
    e?.preventDefault()

    const { data } = await tns.name(nameInput).query(builder =>
      builder
        // -- Registrar
        .isAvailable()
        .getImage()
        .getExpires()
        .getOwner()
        .getGracePeriod()
        // -- Registry
        .getRegistrar()
        .getEditor()
        .getResolver()
        .getTTL()
        // -- Resolver
        .getTerraAddress()
        .getContentHash()
        .getTextData('email', 'url', 'avatar', 'description', 'twitter', 'github')
    )

    setData(data)
    setName(nameInput)
  }

  useEffect(() => {
    fetchData()
  }, [])

  const handleSetTextData = async (key: string) => {
    const value = prompt(`Set "${key}" to`)

    if (value) {
      const executeMsg = await tns.name(nameInput).setTextData(key, value)

      await connectedWallet.post({
        msgs: [executeMsg],
        fee: new Fee(500000, '100000uusd'),
      })
    }
  }

  const handleRegisterClick = async () => {
    const tns = new TNS()
    const tnsName = tns.name('lunatics.ust')

    const secret = 'any-string-can-be-used-as-a-secret'

    // Create a commitment
    const commitment = tnsName.commitment({ secret })

    // Broadcast "commit" transaction to blockchain
    const commitTxResult = await connectedWallet.post({
      msgs: [tnsName.commit(commitment)],
      fee: new Fee(200000, '50000uusd'),
    })

    // Wait until "commit" transaction succeeded
    await delay(5 * 1000)

    /**
     * Show waiting dialog, users will need to wait for AT LEAST 20s,
     * before they can register. You can also use getCommitmentTimestamp
     * to calculate how long the users actually need to wait.
     *
     * @example
     * const timestamp = await tnsName.getCommitmentTimestamp(commitment)
     */
    await delay(20 * 1000)

    const ONE_YEAR = 60 * 60 * 24 * 365

    /**
     * Rent price for 1 year in uusd (hex string).
     * If you want to display it in UST, you need to divide it by 1e6.
     *
     * @example
     * rentPrice = "16000000"
     * displayRentPrice = Number(rentPrice) / 1e6
     */
    const rentPrice = await tnsName.getRentPrice(ONE_YEAR)

    const tax = await lcdClient.utils.calculateTax(new Coin('uusd', rentPrice))

    const registerTxResult = await connectedWallet.post({
      msgs: [await tnsName.register({ duration: ONE_YEAR, secret })],
      // Gas + Tax since this transaction also send coins.
      fee: new Fee(2000000, new Coins('500000uusd').add(tax)),
    })
  }

  return (
    <div style={{ maxWidth: 800, margin: 'auto' }}>
      <h1>Terra Name Service</h1>

      <div style={{ marginBottom: 40 }}>
        {connectedWallet ? (
          <div>
            {connectedWallet.terraAddress}
            <button style={{ marginLeft: 8 }} onClick={disconnect}>
              Disconnect
            </button>
          </div>
        ) : (
          <button onClick={() => connect(ConnectType.EXTENSION)}>Connect</button>
        )}
      </div>

      <div>
        <button onClick={handleRegisterClick}>Register</button>
      </div>

      <form onSubmit={fetchData}>
        <b>Name</b>:{' '}
        <input
          type="text"
          value={nameInput}
          onChange={e => setNameInput(e.target.value)}
        />{' '}
        <button type="submit">Fetch</button>
      </form>

      {data && (
        <table>
          <thead>
            <tr>
              <th style={{ minWidth: 120, width: 120 }}>Property</th>
              <th>Value</th>
              <th style={{ width: 120 }}>Actions</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>Name</td>
              <td>{name}</td>
              <td />
            </tr>
            <tr>
              <td>Available</td>
              <td>{String(data.isAvailable)}</td>
              <td />
            </tr>
            <tr>
              <td>Image</td>
              <td>
                {data.image && <img src={data.image} alt={nameInput} width={300} />}
              </td>
              <td />
            </tr>
            <tr>
              <td>Exp. Date</td>
              <td>
                {Boolean(data.expires) &&
                  dayjs(data.expires).format('DD MMMM YYYY [at] HH:mm [(UTC]Z[)]')}
              </td>
              <td>
                <button>Renew</button>
              </td>
            </tr>
            <tr>
              <td>Grace Period</td>
              <td>
                {Boolean(data.gracePeriod) &&
                  dayjs.duration(data.gracePeriod, 'seconds').asDays() + ' days'}
              </td>
              <td>
                <button>Renew</button>
              </td>
            </tr>
            <tr>
              <td>Owner</td>
              <td>{data.owner}</td>
              <td>
                <button>Transfer</button>
              </td>
            </tr>
            <tr>
              <td>Editor</td>
              <td>{data.editor}</td>
              <td>
                <button>Set</button>
              </td>
            </tr>
            <tr>
              <td>Resolver</td>
              <td>{data.resolver}</td>
              <td>
                <button>Set</button>
              </td>
            </tr>
            <tr>
              <td>TTL</td>
              <td>{data.ttl}</td>
              <td>
                <button>Set</button>
              </td>
            </tr>
            <tr>
              <td>Address</td>
              <td>{data.terraAddress}</td>
              <td>
                <button>Set</button>
              </td>
            </tr>
            <tr>
              <td>Content Hash</td>
              <td>{data.contentHash}</td>
              <td>
                <button>Set</button>
              </td>
            </tr>
            <tr>
              <td>Text Data</td>
              <td colSpan={2} style={{ padding: 0 }}>
                <table style={{ margin: 0, borderCollapse: 'separate' }}>
                  <tbody>
                    {Object.keys(data.textData).map(key => (
                      <tr key={key}>
                        <td width={120}>{key}</td>
                        <td>{data.textData[key]}</td>
                        <td>
                          <button onClick={() => handleSetTextData(key)}>Set</button>
                        </td>
                      </tr>
                    ))}
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      )}
    </div>
  )
}
