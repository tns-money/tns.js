import { LCDClient, MnemonicKey, Fee } from '@terra-money/terra.js'
import { TNS } from 'tns.js'

require('util').inspect.defaultOptions.depth = null

const main = async () => {
  const lcdClient = new LCDClient({ URL: '', chainID: '' })
  const wallet = lcdClient.wallet(new MnemonicKey({ mnemonic: '' }))
  const tns = new TNS({ walletAddress: wallet.key.accAddress })
  const secret = 'secret' + Math.random()

  const tnsName = tns.name('tomato.ust')

  const commitment = tnsName.commitment({ secret })
  const commitMsg = tnsName.commit(commitment)

  const tx = await wallet.createAndSignTx({
    msgs: [commitMsg],
    fee: new Fee(200000, '50000uusd'),
  })

  await lcdClient.tx.broadcastAsync(tx)
}

main()
