export const log = (...args: any[]) => {
  const title = args[0]
  console.log(`-------- ${title} --------`)
  console.log(...args.slice(1))
  console.log()
}
