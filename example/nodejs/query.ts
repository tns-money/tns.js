import { QueryBuilder, TNS } from 'tns.js'
import { log } from './log'

require('util').inspect.defaultOptions.depth = null

const main = async () => {
  const tns = new TNS({ network: 'mainnet' })
  const name = tns.name('tomato.ust')

  log(
    'get reverse record',
    await tns.getName('terra1xh5l4k8uml7ljfq3p5hutesdrsr9g9w4hpsc8t')
  )

  log(
    'batch tns query',
    await tns
      .query([
        new QueryBuilder('tomato.ust')
          .getOwner()
          .isAvailable()
          .getExpires()
          .getTerraAddress()
          .getResolver(),
        new QueryBuilder('banana.ust')
          .getOwner()
          .getEditor()
          .getTextData('github', 'url', 'twitter')
          .getTerraAddress(),
        new QueryBuilder('thisnameisavailable.ust')
          .isAvailable()
          .getOwner()
          .getImage()
          .getExpires()
          .getRentPrice(),
      ])
      .then(res => res.data)
  )

  log(
    'batch name query',
    await name
      .query(builder =>
        builder
          .getExpires()
          .getTerraAddress()
          .isAvailable()
          .getOwner()
          .getEditor()
          .getImage()
          .getTextData('github', 'url')
      )
      .then(res => res.data)
  )

  /* Registrar */
  log('isAvailable', await name.isAvailable())
  log('getOwner', await name.getOwner())
  log('getExpires', await name.getExpires())
  log('getGracePeriod', await name.getGracePeriod())
  log('getImage', await name.getImage())
  log('getConfig', await name.getConfig())

  /* Registry */
  log('getRegistrar', await name.getRegistrar())
  log('getEditor', await name.getEditor())
  log('getResolver', await name.getResolver())
  log('getTTL', await name.getTTL())

  /* Resolver */
  log('getTerraAddress', await name.getTerraAddress())
  log('getTextData("email")', await name.getTextData('email'))
  log('getTextData("url")', await name.getTextData('url'))
  log('getTextData("avatar")', await name.getTextData('avatar'))
  log('getTextData("description")', await name.getTextData('description'))
  log('getTextData("twitter")', await name.getTextData('twitter'))
  log('getTextData("github")', await name.getTextData('github'))
  log('getContentHash', await name.getContentHash())
  log('getAddress', await name.getAddress(0x8000014a))

  /* Controller */
  log('getRentPrice', await name.getRentPrice())
}

main()
